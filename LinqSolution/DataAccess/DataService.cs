﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DataAccess.Models;

namespace DataAccess
{
    public class DataService
    {
        public List<PersonalInformation> GetPersonalInformations()
        {
            const string fileName = "Resources/fake_what-they-know-about-you.csv";
            const char separator = ',';

            Func<string[], PersonalInformation> projectionFunc = (columns) => new PersonalInformation
            {
                Id = int.Parse(columns[0]),
                Basic = new BasicInformation
                {
                    Email = columns[1],
                    Gender = columns[2] == "Female" ? Gender.Female : Gender.Male,
                    IpAddress = columns[9]
                },
                Localization = new LocalizationInformation
                {
                    Country = columns[5],
                    Currency = columns[6],
                    PrimaryLanguage = columns[8]
                },
                Extended = new ExtendedInformation
                {
                    ShirtSize = columns[3],
                    Company = columns[4],
                    JobTitle = columns[7],
                    FavoriteSlogan = columns[10]
                }
            };

            return ReadCsvFile(fileName, separator, projectionFunc);

        }
        public List<WifiAccessPoint> GetWifiAccessPoints()
        {
            const string fileName = "Resources/wifi-gdansk.csv";
            const char separator = ',';
            Func<string[], WifiAccessPoint> projectionFunc = (columns) => new WifiAccessPoint
            {
                Id = columns[0],
                Localization = columns[1],
                CoordinateX = float.Parse(columns[2]),
                CoordinateY = float.Parse(columns[3])
            };

            return ReadCsvFile(fileName, separator, projectionFunc);
        }

        public List<WifiUsage> GetWifiUsageAugust()
        {
            const string fileName = "Resources/wifi-08-2016.csv";
            const char separator = ';';
            Func<string[], WifiUsage> projectionFunc = (columns) => new WifiUsage
            {
                Id = columns[0],
                Localization = columns[1],
                UsersNumber = int.Parse(columns[2]),
                TransferIncome = int.Parse(columns[3]),
                TransferOutcome = int.Parse(columns[4])
            };

            return ReadCsvFile(fileName, separator, projectionFunc);
        }

        public List<WifiUsage> GetWifiUsageJuly()
        {
            const string fileName = "Resources/wifi-07-2016.csv";
            const char separator = ';';
            Func<string[], WifiUsage> projectionFunc = (columns) => new WifiUsage
            {
                Id = columns[0],
                Localization = columns[1],
                UsersNumber = int.Parse(columns[2]),
                TransferIncome = int.Parse(columns[3]),
                TransferOutcome = int.Parse(columns[4])
            };

            return ReadCsvFile(fileName, separator, projectionFunc);
        }

        public List<DemographyMeasurementUnit> GetDemographyMeasurements()
        {
            const string fileName = "Resources/gdansk-demography-2014.csv";
            const char separator = ',';
            Func<string[], DemographyMeasurementUnit> projectionFunc = (columns) => new DemographyMeasurementUnit
            {
                SplitUnit = columns[0],
                ChildrenNumber = int.Parse(columns[1]),
                AdultsNumber = int.Parse(columns[2]),
                AverageAgeNumber = int.Parse(columns[3]),
                ElderlyNumber = int.Parse(columns[4])
            };

            return ReadCsvFile(fileName, separator, projectionFunc);
        }

        private static List<T> ReadCsvFile<T>(string fileName, char separator, Func<string[], T> projectionFunc)
        {
            return File.ReadAllLines(fileName)
                .Skip(1)
                .Where(line => line.Any())
                .Select(line =>
                {
                    var columns = line.Split(separator);
                    return projectionFunc(columns);
                }).ToList();
        }
    }

    public class PersonalInformation
    {
        public int Id { get; set; }
        public BasicInformation Basic { get; set; }
        public ExtendedInformation Extended { get; set; }
        public LocalizationInformation Localization { get; set; }
    }

    public class LocalizationInformation
    {
        public string Country { get; set; }
        public string PrimaryLanguage { get; set; }
        public string Currency { get; set; }
    }

    public class ExtendedInformation
    {
        public string Company { get; set; }
        public string ShirtSize { get; set; }
        public string JobTitle { get; set; }
        public string FavoriteSlogan { get; set; }
    }

    public class BasicInformation
    {
        public string Email { get; set; }
        public Gender Gender { get; set; }
        public string IpAddress { get; set; }
    }

    public enum Gender
    {
        Female = 1,
        Male = 2
    }
}